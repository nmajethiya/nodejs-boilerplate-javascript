const app = require("./app");
const port = 3000;
(async () => {
  try {
    app.listen(port, () => {
      console.log(`Server started on port ${port}`);
    });
  } catch (error) {
    console.log(`Error while starting server ${error}`);
    process.exit(1);
  }
})();
