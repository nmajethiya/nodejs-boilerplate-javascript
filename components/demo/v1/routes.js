const {Router} = require("express");
const validation = require("./validation");
const controller = require("./controller");

const router = Router();
router.get("/test", [validation.test], (req, res) => {
  controller.test(req, res);
});

module.exports = router;
