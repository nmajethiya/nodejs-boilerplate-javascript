class Controller {
  test(req, res, next) {
    return res.status(200).json({
      status: 200,
      message: "Test success",
    });
  }
}

const controller = new Controller();
module.exports = controller;
