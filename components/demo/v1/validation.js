class Validation {
  test(req, res, next) {
    let errors = {};
    if (Object.keys(errors).length > 0) {
      return res.status(422).json({
        status: 422,
        message: errors[Object.keys(errors)[0]],
        payload: {
          error: errors[Object.keys(errors)[0]],
        },
      });
    } else {
      next();
    }
  }
}

const validation = new Validation();
module.exports = validation;
