const express = require("express");
const demoRouteV1 = require("./components/demo/v1");
const app = express();

app.use("/demo", demoRouteV1);

// Base route to health check
app.get("/health", (req, res) => {
  console.log("Health Check");
  return res.status(200).send("healthy");
});

// Handle invalid Route
app.all("/*", (req, res) => {
  console.log("Invalid Route Handler");
  return res.status(400).json({
    status: 400,
    message: `Bad Request`,
  });
});

module.exports = app;
